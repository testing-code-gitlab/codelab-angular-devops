# 1
```
browsers: ['Chrome'],
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
}
```

# 2

```
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```

# 3

```
image: node:latest

stages:
  - install
  - test
  - build
  - deploy

```


# 4 

```

install:
  stage: install
  script:
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/

```

# 5
```
tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - #install chrome
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'

```

# 6
```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
```

# 7
```
build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/codelab-angular-devops/

```

# 8

```
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/codelab-angular-devops/* ./public/
    - cp ./public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master
```
# CodelabAngularDevops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.0.
